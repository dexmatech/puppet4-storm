# Class: apache_storm::config
# ===========================
class apache_storm::config inherits apache_storm {

  notice($apache_storm::zookeeper_servers)
  # Hash to render in template
  $zookeeper_servers = $apache_storm::zookeeper_servers
  $nimbus_seeds      = $apache_storm::nimbus_seeds
  $supervisor_ports  = $apache_storm::supervisor_ports
  $worker_options    = $apache_storm::worker_options
  $topology_workers  = $apache_storm::topology_workers

  # create config file
  file { "${apache_storm::config_path}/storm.yaml":
    ensure  => $apache_storm::ensure,
    content => template("${module_name}/storm.yaml.erb"),
    owner   => $apache_storm::user,
    group   => $apache_storm::group,
    mode    => '0644',
  }

  # create worker.xml file

  file { "${apache_storm::config_path}/worker.xml":
    ensure  => file,
    source => "puppet:///modules/${module_name}/worker.xml",
    owner   => $apache_storm::user,
    group   => $apache_storm::user,
    mode    => '0644'
  }

  file { "${apache_storm::config_path}/cluster.xml":
    ensure  => file,
    source => "puppet:///modules/${module_name}/cluster.xml",
    owner   => $apache_storm::user,
    group   => $apache_storm::user,
    mode    => '0644'
  }
}
